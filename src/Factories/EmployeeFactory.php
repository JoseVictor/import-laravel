<?php

namespace Src\Factories;

use Src\Builders\EmployeeBuilder;
use Src\Models\EmployeeModel;

class EmployeeFactory
{

    /**
     * 
     * @param \Src\Builders\EmployeeBuilder $builder
     * @return \Src\Models\EmployeeModel
     */
    public function model(EmployeeBuilder $builder)
    {
        return new EmployeeModel([
            EmployeeModel::COLUMN_ID => $builder->getId(),
            EmployeeModel::COLUMN_NAME => $builder->getName(),
            EmployeeModel::COLUMN_COMPANY_ID => $builder->getCompanyId(),
            EmployeeModel::COLUMN_PROCESSED_AT => $builder->getProcessedAt(),
            EmployeeModel::COLUMN_STATUS => $builder->getStatus()
        ]);
    }

}
