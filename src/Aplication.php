<?php

class Application extends \Illuminate\Foundation\Application
{

    public function path()
    {
        return $this->basePath . DIRECTORY_SEPARATOR . 'src';
    }

}
