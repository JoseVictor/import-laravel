<?php

namespace Src\Validators;

use Src\Validators\AbstractValidator;
use Src\Interfaces\ValidatorInterface;

class CompanyValidator extends AbstractValidator implements ValidatorInterface
{

    const RULES = array(
        'id' => array('numeric')
    );

    /**
     * 
     * @see \Src\Validators\AbstractValidator::getRules()
     */
    public static function getRules()
    {
        return self::RULES;
    }

}
