<?php

namespace Src\Validators;

use Src\Validators\AbstractValidator;
use Src\Interfaces\ValidatorInterface;
use Src\Support\Constants;
use Illuminate\Validation\Rule;
use Src\Validators\CompanyValidator;

class EmployeeValidator extends AbstractValidator implements ValidatorInterface
{

    const RULES = array(
        'name' => array('required', 'string'),
        'id' => array('numeric'),
        'company_id' => array('numeric'),
        'processed_at' => array(/* 'date_format:Y-m-d', */ 'date', 'required'),
        'status' => array('required')
    );

    /**
     * 
     * @see \Src\Validators\AbstractValidator::getRules()
     */
    public static function getRules()
    {
        $rules = self::RULES;
        $rules['status'][] = Rule::in([Constants::TYPE_EVENT_INCLUDE, Constants::TYPE_EVENT_EXCLUDE]);
        $rules['company_id'] = CompanyValidator::getRules()['id'];

        return $rules;
    }

}
