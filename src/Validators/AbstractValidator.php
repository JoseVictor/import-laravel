<?php

namespace Src\Validators;

use Validator;

abstract class AbstractValidator
{

    /**
     * 
     * @see \Src\Interfaces\ValidatorInterface
     */
    abstract public static function getRules();

    /**
     * 
     * @see \Src\Interfaces\ValidatorInterface
     */
    public static function make(array $data, array $rules)
    {
        return Validator::make($data, $rules);
    }

}
