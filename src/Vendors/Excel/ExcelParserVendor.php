<?php

namespace Src\Vendors\Excel;

use Src\Interfaces\Excel\ExcelParserInterface;
use Maatwebsite\Excel\Readers\LaravelExcelReader;

class ExcelParserVendor
{

    /**
     *
     * @var \Src\Interfaces\Excel\ExcelParserInterface 
     */
    private $parser;

    public function __construct(ExcelParserInterface $parser)
    {
        $this->parser = $parser;
    }

    /**
     * 
     * @param \Maatwebsite\Excel\Readers\LaravelExcelReader $file
     * @return mixed
     */
    public function parse(LaravelExcelReader $file)
    {
        return $this->parser->parse($file);
    }

}
