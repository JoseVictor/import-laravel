<?php

namespace Src\Vendors\Excel;

use Src\Interfaces\Excel\ExcelValidatorInterface;
use Src\Interfaces\Handlers\ValidationHandlerInterface;
use Src\Interfaces\Excel\ExcelParserInterface;

class ExcelValidatorVendor
{

    /**
     *
     * @var \Src\Interfaces\Excel\ExcelValidatorInterface 
     */
    private $validator;

    /**
     *
     * @var \Src\Interfaces\Handlers\ValidationHandlerInterface 
     */
    private $handler;

    /**
     * 
     * @param \Src\Interfaces\Excel\ExcelValidatorInterface       $validator
     * @param \Src\Interfaces\Handlers\ValidationHandlerInterface $handler
     */
    public function __construct(ExcelValidatorInterface $validator, ValidationHandlerInterface $handler)
    {
        $this->validator = $validator;
        $this->handler = $handler;
    }

    /**
     * 
     * @param \Src\Interfaces\Excel\ExcelParserInterface $parsed
     * @return mixed
     */
    public function validate(ExcelParserInterface $parsed)
    {
        return $this->handler->handle($this->make($parsed));
    }

    /**
     * 
     * @param \Src\Interfaces\Excel\ExcelParserInterface $parsed
     * @return mixed
     */
    public function make(ExcelParserInterface $parsed)
    {
        $validator = $this->validator;

        return $validator::make($parsed->get()['data'], $validator::getRules());
    }

}
