<?php

namespace Src\Vendors\Excel;

use Maatwebsite\Excel\Files\ExcelFile;
use Maatwebsite\Excel\Excel;
use Illuminate\Foundation\Application;
use Src\Vendors\Excel\ExcelParserVendor;
use Src\Vendors\Excel\ExcelValidatorVendor;
use Exception;
use Closure;

class ExcelFileVendor extends ExcelFile
{

    /**
     *
     * @var array 
     */
    private $paths = [];

    /**
     *
     * @var \Src\Vendors\Excel\ExcelValidatorVendor 
     */
    private $validator;

    /**
     *
     * @var \Src\Vendors\Excel\ExcelParserVendor 
     */
    private $parser;

    /**
     *
     * @var string 
     */
    private $currentPath;

    /**
     * 
     * @param \Illuminate\Foundation\Application      $app
     * @param \Maatwebsite\Excel\Excel                $excel
     * @param \Src\Vendors\Excel\ExcelParserVendor    $parser
     * @param \Src\Vendors\Excel\ExcelValidatorVendor $validator
     */
    public function __construct(
            Application $app,
            Excel $excel,
            ExcelParserVendor $parser, 
            ExcelValidatorVendor $validator
    ) {
        $this->parser = $parser;
        $this->validator = $validator;

        try {
            parent::__construct($app, $excel);
        } catch (Exception $exc) {
            
        }
    }

    /**
     * 
     * @param array $paths
     * @return \Src\Vendors\Excel\ExcelFileVendor
     */
    public function setPaths(array $paths)
    {
        $this->paths = $paths;
        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getFile()
    {
        return $this->currentPath;
    }

    /**
     * 
     * @param \Closure $callback
     * @throws \Exception
     */
    public function loadFiles(Closure $callback)
    {
        if (empty($this->paths)) {
            throw new Exception('No files to process');
        }

        foreach ($this->paths as $path) {

            $this->currentPath = $path;
            $callback($this->file = $this->loadFile());
        }
    }

    /**
     * 
     * @param mixed $file
     * @return mixed
     */
    public function parse($file)
    {
        return $this->parser->parse($file);
    }

    /**
     * 
     * @param mixed $parsed
     * @return mixed
     */
    public function validate($parsed)
    {
        return $this->validator->validate($parsed);
    }

}
