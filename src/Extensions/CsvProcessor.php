<?php

namespace Src\Extensions;

use Exception;
use Src\Services\EmployeeService;
use Src\Interfaces\Excel\ExcelRowInterface;
use Src\Support\Constants;
use Src\Traits\LogsTrait;

class CsvProcessor
{

    use LogsTrait;

    /**
     *
     * @var \Src\Services\EmployeeService 
     */
    private $employeeService;

    /**
     * 
     * @param \Src\Services\EmployeeService $employeeService
     */
    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    /**
     * 
     * @param \Src\Interfaces\Excel\ExcelRowInterface $row
     */
    public function process(ExcelRowInterface $row)
    {
        try {
            switch ($row->get('type')) {
                case Constants::TYPE_EVENT_EXCLUDE :
                    $this->caseExcludeEmployee($row);
                    break;
                case Constants::TYPE_EVENT_INCLUDE:
                    $this->caseIncludeEmployee($row);
                    break;
                default:
                    throw new Exception(Constants::MSG_EXCEPTION_INVALID_TYPE);
            }
        } catch (Exception $exc) {
            $this->logs[] = $exc->getMessage();
        }
    }

    /**
     * 
     * @param \Src\Interfaces\Excel\ExcelRowInterface $row
     * @throws Exception
     */
    private function caseExcludeEmployee(ExcelRowInterface $row)
    {
        $buildedEmployee = $row->get('employee');

        $employee = $this->employeeService->find($buildedEmployee->getId());
        if (empty($employee)) {
            throw new Exception('Could not find employee :' . $buildedEmployee->getId());
        }

        $employee->status = Constants::STATUS_EXCLUDE;
        $employee->save();
    }

    /**
     * 
     * @param \Src\Interfaces\Excel\ExcelRowInterface $row
     */
    private function caseIncludeEmployee(ExcelRowInterface $row)
    {
        $buildedEmployee = $row->get('employee');

        $data = $buildedEmployee->toArray();
        $data['status'] = Constants::STATUS_INCLUDE;

        $this->employeeService->updateOrCreate(['id' => $buildedEmployee->getId()], $data);
    }

}
