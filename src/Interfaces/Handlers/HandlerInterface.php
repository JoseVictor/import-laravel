<?php

namespace Src\Interfaces\Handlers;

interface HandlerInterface
{

    /**
     * 
     * @param array $data
     */
    public function handle(array $data);
}
