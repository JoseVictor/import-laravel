<?php

namespace Src\Interfaces\Excel;

use Maatwebsite\Excel\Readers\LaravelExcelReader;

interface ExcelParserInterface
{

    /**
     * 
     * @param LaravelExcelReader $reader
     * @return \Src\Companies\DB1\Excel\ExcelParserVendor
     */
    public function parse(LaravelExcelReader $reader);

    /**
     * @return array
     */
    public function get();
}
