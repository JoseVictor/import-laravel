<?php

namespace Src\Interfaces\Excel;

interface ExcelRowInterface
{

    /**
     * 
     * @param string $key
     * @return \Src\Models\AbstractModel
     */
    public function get(string $key = '');

    /**
     * @param string $line
     * @return \Src\Companies\DB1\Excel\ExcelRow
     */
    public function getInstance(string $line);

    /**
     * 
     * @param string $line
     * @return array
     */
    public function generateColumns(string $line);

    /**
     * 
     * @param array $columns
     * @return array
     */
    public function defineValues(array $columns);
}
