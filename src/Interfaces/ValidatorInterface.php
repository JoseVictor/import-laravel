<?php

namespace Src\Interfaces;

interface ValidatorInterface
{

    /**
     * @return array
     */
    public static function getRules();

    /**
     * 
     * @param array $data
     * @param array $rules
     * @return \Illuminate\Validation\Validator
     */
    public static function make(array $data, array $rules);
}
