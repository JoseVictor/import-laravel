<?php

namespace Src\Console\Commands;

use Illuminate\Console\Command;
use Src\Extensions\CsvProcessor;
use Src\Services\LogService;
use Exception;
use Src\Services\FileService;
use Src\Support\Constants;
use Src\Traits\LogsTrait;

class ImportCommand extends Command
{

    use LogsTrait;

    /**
     *
     * @var \Src\Services\FileService 
     */
    private $fileService;

    /**
     *
     * @var \Src\Services\LogService 
     */
    private $logService;

    /**
     *
     * @var array 
     */
    private $processors;

    /**
     *
     * @var string 
     */
    protected $signature = 'command:import';

    /**
     *
     * @var string 
     */
    protected $description = 'Command description';

    /**
     * 
     * @param \Src\Services\FileService    $fileService
     * @param \Src\Extensions\CsvProcessor $csvProcessor
     * @param \Src\Services\LogService     $logService
     */
    public function __construct(FileService $fileService, CsvProcessor $csvProcessor, LogService $logService)
    {
        parent::__construct();

        $this->fileService = $fileService;
        $this->logService = $logService;

        $this->processors = [
            FileService::CSV => $csvProcessor
        ];
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $extensions = [FileService::CSV];

        foreach ($extensions as $extension) {

            try {

                $this->fileService->configure($extension);
                $this->fileService->loadFilesToProcess(function($reader) use ($extension) {

                    try {

                        $responseParsed = $this->fileService->parse($reader);
                        $responseValidate = $this->fileService->validate($responseParsed);

                        if (!empty($logs = $responseValidate['logs'])) {
                            $this->appendLogs($logs);
                        }

                        $this->fileService->move($reader);

                        if (empty($responseValidate['valids'])) {
                            throw new Exception(Constants::MSG_EXCEPTION_NOT_FOUND_DATA_VALID_TO_PROCESS);
                        }

                        foreach ($responseValidate['valids'] as $row) {
                            $this->processors[$extension]->process($row);
                        }

                        if (!empty($logs = $this->processors[$extension]->getLogs())) {
                            $this->appendLogs($logs);
                        }
                    } catch (Exception $exc) {
                        $this->appendLogs([$exc->getMessage()]);
                    }
                });
            } catch (Exception $exc) {
                $this->appendLogs([$exc->getMessage()]);
            }
        }

        $this->handleFailure();
    }

    /**
     * 
     * @param array $logs
     */
    private function appendLogs(array $logs = [])
    {
        if (!empty($logs)) {
            $this->logs = array_merge($this->logs, $logs);
        }
    }

    /**
     * @throws \Exception
     */
    private function handleFailure()
    {
        $logService = $this->logService;

        foreach ($this->getLogs() as $log) {
            $logService->execute($logService::FAIL, $log);
        }
    }

}
