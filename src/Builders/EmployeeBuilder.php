<?php

namespace Src\Builders;

use Src\Factories\EmployeeFactory;
use Src\Support\Constants;
use Exception;
use Carbon\Carbon;

class EmployeeBuilder
{

    /**
     *
     * @var string|null
     */
    private $name;

    /**
     *
     * @var \Src\Factories\EmployeeFactory 
     */
    private $factory;

    /**
     *
     * @var int|null
     */
    private $id;

    /**
     *
     * @var string|null
     */
    private $status;

    /**
     *
     * @var string|null
     */
    private $processedAt;

    /**
     *
     * @var int|null
     */
    private $companyId;

    /**
     * 
     * @param \Src\Factories\EmployeeFactory $factory
     */
    public function __construct(EmployeeFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * 
     * @param string|null $name
     * @return \Src\Builders\EmployeeBuilder
     */
    public function setName($name)
    {
        $this->name = (is_string($name)) ? $name : null;
        return $this;
    }

    /**
     * 
     * @param int|null $id
     * @return \Src\Builders\EmployeeBuilder
     */
    public function setId($id)
    {
        $this->id = (is_numeric($id)) ? $id : null;
        return $this;
    }

    /**
     * 
     * @param string|null $date
     * @param string      $inputFormat
     * @param string      $outputFormat
     * @return \Src\Builders\EmployeeBuilder
     */
    public function setProcessedAt($date, string $inputFormat = 'Y-m-d', string $outputFormat = 'Y-m-d')
    {
        try {
            $date = Carbon::createFromFormat($inputFormat, $date)->format($outputFormat);
        } catch (Exception $exc) {
            $date = null;
        }

        $this->processedAt = $date;
        return $this;
    }

    /**
     * 
     * @param int|null $id
     * @return \Src\Builders\EmployeeBuilder
     */
    public function setCompanyId($id)
    {
        $this->companyId = (is_numeric($id)) ? $id : null;
        return $this;
    }

    /**
     * 
     * @param string|null $type
     * @return \Src\Builders\EmployeeBuilder
     */
    public function setStatusByType($type)
    {
        if (!in_array($type, [Constants::TYPE_EVENT_INCLUDE, Constants::STATUS_EXCLUDE])) {
            return $this;
        }

        $this->status = $type;
        return $this;
    }

    /**
     * 
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * 
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * 
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * 
     * @return string|null
     */
    public function getProcessedAt()
    {
        return $this->processedAt;
    }

    /**
     * 
     * @return int|null
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * 
     * @return \Src\Models\EmployeeModel
     */
    public function build()
    {
        return $this->factory->model($this);
    }

}
