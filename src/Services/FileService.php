<?php

namespace Src\Services;

use Src\Vendors\Excel\ExcelFileVendor;
use Exception;
use Closure;

class FileService
{

    const CSV = 'CSV';

    /**
     *
     * @var string 
     */
    private $type;

    /**
     *
     * @var \Maatwebsite\Excel\Files\ExcelFile 
     */
    private $vendor;

    /**
     *
     * @var string
     */
    private $extension;

    /**
     *
     * @var array 
     */
    private $vendors;

    /**
     *
     * @var array 
     */
    private $types;

    /**
     * 
     * @param \Src\Vendors\Excel\ExcelFileVendor $excelVendor
     */
    public function __construct(ExcelFileVendor $excelVendor)
    {
        $this->vendors = [
            self::CSV => $excelVendor
        ];

        $this->types = [
            self::CSV
        ];
    }

    /**
     * 
     * @param string $type
     * @return boolean
     */
    public function configure(string $type)
    {
        if (!in_array($type, $this->types)) {
            return false;
        }

        $this->vendor = (!isset($this->vendors[$type])) ? null : $this->vendors[$type];
        $this->type = $type;
        $this->extension = strtolower($this->type);

        return true;
    }

    /**
     * 
     * @param \Closure $callback
     * @throws \Exception
     */
    public function loadFilesToProcess(Closure $callback)
    {
        if (empty($this->vendor)) {
            throw new Exception('Configuration is required before load files');
        }

        $this->vendor
                ->setPaths($this->getAddressFilesToProcess())
                ->loadFiles($callback);
    }

    /**
     * 
     * @return array
     * @throws \Exception
     */
    public function getAddressFilesToProcess()
    {
        if (empty($this->extension)) {
            throw new Exception('Configuration is required before get address files');
        }

        $folders = self::getPathFilesFolders();

        return glob($folders['to_process'] . '*.' . $this->extension);
    }

    /**
     * 
     * @param mixed $file
     * @return mixed
     * @throws \Exception
     */
    public function parse($file)
    {
        if (empty($this->vendor)) {
            throw new Exception('Configuration is required before parse file');
        }

        return $this->vendor->parse($file);
    }

    /**
     * 
     * @param mixed $file
     * @return mixed
     * @throws \Exception
     */
    public function validate($file)
    {
        if (empty($this->vendor)) {
            throw new Exception('Configuration is required before validate file');
        }

        return $this->vendor->validate($file);
    }

    /**
     * 
     * @param mixed $file
     */
    public function move($file)
    {
        $oldFile = clone $file;
        $oldFileName = $oldFile->getFileName();

        $new = str_replace($oldFileName, microtime() . '-' . $oldFileName, $oldFile->file);
        $new = str_replace('to-process', 'processeds', $new);

        @rename($oldFile->file, $new);
    }

    /**
     * 
     * @param boolean $storagePath
     * @return array
     */
    public static function getPathFilesFolders($storagePath = true)
    {
        $ds = DIRECTORY_SEPARATOR;
        $commonPath = 'app' . $ds . 'public' . $ds . 'docs' . $ds;

        $toProcess = $commonPath . 'files-to-process' . $ds;
        $processeds = $commonPath . 'files-processeds' . $ds;

        if ($storagePath) {
            $toProcess = storage_path($toProcess);
            $processeds = storage_path($processeds);
        }

        return [
            'to_process' => $toProcess,
            'processeds' => $processeds
        ];
    }

}
