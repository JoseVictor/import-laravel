<?php

namespace Src\Services;

use Src\Services\AbstractService;
use Src\Repositories\EmployeeRepository;
use Src\Builders\EmployeeBuilder;

class EmployeeService extends AbstractService
{

    /**
     *
     * @var \Src\Builders\EmployeeBuilder 
     */
    private $builder;

    /**
     * 
     * @param \Src\Repositories\EmployeeRepository $repository
     * @param \Src\Builders\EmployeeBuilder        $builder
     */
    public function __construct(EmployeeRepository $repository, EmployeeBuilder $builder)
    {
        parent::__construct($repository);

        $this->builder = $builder;
    }

    /**
     * 
     * @param int|null    $employeeId
     * @param string|null $employeeName
     * @param string|null $type
     * @param int|null    $companyId
     * @param string|null $processedAt
     * @return \Src\Models\EmployeeModel
     */
    public function buildEmployee($employeeId = null, $employeeName = null, $type = null, $companyId = null, $processedAt = null)
    {
        $this->builder
                ->setId($employeeId)
                ->setName($employeeName)
                ->setStatusByType($type)
                ->setProcessedAt($processedAt, 'd/m/Y')
                ->setCompanyId($companyId);

        return $this->builder->build();
    }

}
