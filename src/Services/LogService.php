<?php

namespace Src\Services;

use Src\Repositories\LogRepository;
use Src\Services\AbstractService;
use Illuminate\Support\Facades\Log;
use Exception;

class LogService extends AbstractService
{

    const FAIL = 'FAIL';
    const SUCCESS = 'SUCCESS';

    /**
     * 
     * @param \Src\Repositories\LogRepository $repository
     */
    public function __construct(LogRepository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * 
     * @param string       $type
     * @param string|array $data
     */
    public function execute(string $type, $data = null)
    {
        if (!$this->validateType($type)) {
            return;
        }

        $data = $this->format($type, $data);

        parent::create($data);
        Log::error(json_encode($data, JSON_PRETTY_PRINT));
        print_r($data);
    }

    /**
     * 
     * @param string       $type
     * @param string|array $data
     * @return array
     */
    public function format(string $type, $data = null)
    {
        if (!$this->validateType($type)) {
            return [];
        }

        $args = (is_string($data)) ? ['msg' => $data] : $data;

        return ['type' => $type, 'json' => json_encode($args, JSON_PRETTY_PRINT)];
    }

    /**
     * 
     * @param string $type
     * @return boolean
     * @throws \Exception
     */
    private function validateType(string $type)
    {
        if (!in_array($type, [self::FAIL, self::SUCCESS])) {
            throw new Exception('Log type is invalid, it parameter must be ' . self::FAIL . ' or ' . self::SUCCESS);
        }

        return true;
    }

}
