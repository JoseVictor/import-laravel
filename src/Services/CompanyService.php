<?php

namespace Src\Services;

use Src\Services\AbstractService;
use Src\Repositories\CompanyRepository;

class CompanyService extends AbstractService
{

    /**
     * 
     * @param \Src\Repositories\CompanyRepository $repository
     */
    public function __construct(CompanyRepository $repository)
    {
        parent::__construct($repository);
    }

    /**
     * 
     * @return string
     */
    public static function getCompanyNamespace()
    {
        return 'DB1'; //This value can be defined by the route for example
    }

}
