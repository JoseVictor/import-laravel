<?php

namespace Src\Services;

use Src\Repositories\AbstractRepository;
use Carbon\Carbon;

abstract class AbstractService
{

    /**
     *
     * @var \Src\Repositories\AbstractRepository 
     */
    protected $repository;

    /**
     * 
     * @param \Src\Repositories\AbstractRepository  $repository
     */
    public function __construct(AbstractRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * 
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Model.
     */
    public function find(int $id)
    {
        return $this->repository->find($id);
    }

    /**
     * 
     * @param array $data
     */
    public function create(array $data)
    {
        //$data = array_map('utf8_encode', $data);

        return $this->repository->create($data);
    }

    /**
     * 
     * @param array $condition
     * @param array $data
     */
    public function updateOrCreate(array $condition, array $data)
    {
        return $this->repository->updateOrCreate($condition, $data);
    }

}
