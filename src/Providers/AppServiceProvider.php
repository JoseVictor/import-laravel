<?php

namespace Src\Providers;

use Illuminate\Support\ServiceProvider;
use Src\Services\CompanyService;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->call([$this, 'registerValidator']);
        $this->app->call([$this, 'registerParser']);
        $this->app->call([$this, 'registerHandler']);
        $this->app->call([$this, 'registerRow']);
    }

    /**
     * 
     * @param \Src\Services\CompanyService $companyService
     */
    public function registerValidator(CompanyService $companyService)
    {
        $this->app->bind(\Src\Interfaces\Excel\ExcelValidatorInterface::class, function ($app) use ($companyService) {
            return $app->make('\\Src\\Companies\\' . $companyService::getCompanyNamespace() . '\\Validators\\CsvValidator');
        });
    }

    /**
     * 
     * @param \Src\Services\CompanyService $companyService
     */
    public function registerParser(CompanyService $companyService)
    {
        $this->app->bind(\Src\Interfaces\Excel\ExcelParserInterface::class, function ($app) use ($companyService) {
            return $app->make('\\Src\\Companies\\' . $companyService::getCompanyNamespace() . '\\Excel\\ExcelParserVendor');
        });
    }

    /**
     * 
     * @param \Src\Services\CompanyService $companyService
     */
    public function registerHandler(CompanyService $companyService)
    {
        $this->app->bind(\Src\Interfaces\Handlers\ValidationHandlerInterface::class, function ($app) use ($companyService) {
            return $app->make('\\Src\\Companies\\' . $companyService::getCompanyNamespace() . '\\Handlers\\ValidationHandler');
        });
    }

    /**
     * 
     * @param \Src\Services\CompanyService $companyService
     */
    public function registerRow(CompanyService $companyService)
    {
        $this->app->bind(\Src\Interfaces\Excel\ExcelRowInterface::class, function ($app) use ($companyService) {
            return $app->make('\\Src\\Companies\\' . $companyService::getCompanyNamespace() . '\\Excel\\ExcelRow');
        });
    }

}
