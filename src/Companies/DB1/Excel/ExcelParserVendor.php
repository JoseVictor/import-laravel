<?php

namespace Src\Companies\DB1\Excel;

use Maatwebsite\Excel\Readers\LaravelExcelReader;
use Src\Interfaces\Excel\ExcelParserInterface;
use Src\Companies\DB1\Excel\ExcelRow;

class ExcelParserVendor implements ExcelParserInterface
{

    /**
     *
     * @var array 
     */
    private $data;

    /**
     *
     * @var \Src\Companies\DB1\Excel\ExcelRow 
     */
    private $excelRow;

    /**
     * 
     * @param \Src\Companies\DB1\Excel\ExcelRow $excelRow
     */
    public function __construct(ExcelRow $excelRow)
    {
        $this->excelRow = $excelRow;
    }

    /**
     * 
     * @see \Src\Interfaces\Excel\ExcelParserInterface::parse()
     */
    public function parse(LaravelExcelReader $reader)
    {
        $this->data = array(
            'filename' => $reader->getFileName(),
            'data' => []
        );

        $lines = $this->read($reader);
        if (empty($lines)) {
            return $this;
        }

        //For some reason, the all() and get() method does not return the first line of the csv file ;/
        foreach ($lines as $row) {
            //$simple = array_values($row->toArray());
            $this->data['data'][] = $this->excelRow->getInstance($row);
        }

        return $this;
    }

    /**
     * 
     * @param \Maatwebsite\Excel\Readers\LaravelExcelReader $reader
     * @return array
     */
    public function read(LaravelExcelReader $reader)
    {
        $lineEnding = config('excel.csv.line_ending');

        $file = file_get_contents($reader->file);
        $encodedFile = $this->encode(rtrim($file));

        return (empty($encodedFile)) ? [] : explode($lineEnding, $encodedFile);
    }

    /**
     * 
     * @param string $content
     * @return string
     */
    public function encode(string $content)
    {
        $encoding = mb_detect_encoding($content, 'ISO-8859-1,UTF-8');

        if ($encoding == 'UTF-8') {
            return $content;
        }

        if ($encoding == 'ISO-8859-1') {
            return mb_convert_encoding($content, 'UTF-8', $encoding);
        }

        return $content;
    }

    /**
     * 
     * @see \Src\Interfaces\Excel\ExcelParserInterface::get()
     */
    public function get()
    {
        return $this->data;
    }

}
