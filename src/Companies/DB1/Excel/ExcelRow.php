<?php

namespace Src\Companies\DB1\Excel;

use Src\Services\CompanyService;
use Src\Services\EmployeeService;
use Src\Interfaces\Excel\ExcelRowInterface;

class ExcelRow implements ExcelRowInterface
{

    /**
     *
     * @var array 
     */
    private $struct;

    /**
     *
     * @var string 
     */
    private $line;

    /**
     *
     * @var \Src\Services\EmployeeService 
     */
    private $employeeService;

    /**
     *
     * @var \Src\Services\CompanyService 
     */
    private $companyService;

    /**
     * 
     * @param \Src\Services\EmployeeService  $employeeService
     * @param \Src\Services\CompanyService $companyService
     */
    public function __construct(EmployeeService $employeeService, CompanyService $companyService)
    {
        $this->employeeService = $employeeService;
        $this->companyService = $companyService;
    }

    /**
     * 
     * @see \Src\Interfaces\Excel\ExcelRowInterface::get()
     */
    public function get(string $key = '')
    {
        if (!empty($key)) {
            return (isset($this->struct[$key])) ? $this->struct[$key] : null;
        }

        return $this->struct;
    }

    /**
     * 
     * @param array  $data
     * @param string $line
     * @return \Src\Companies\DB1\Excel\ExcelRow
     */
    public function set(array $data = [], string $line = '')
    {
        $this->struct = $data;
        $this->line = $line;

        return $this;
    }

    /**
     * 
     * @see \Src\Interfaces\Excel\ExcelRowInterface::getInstance()
     */
    public function getInstance(string $line)
    {
        $self = new self($this->employeeService, $this->companyService);

        $columns = $self->generateColumns($line);
        $values = $self->defineValues($columns);
        
        return $self->set($values, $line);
    }

    /**
     * 
     * @see \Src\Interfaces\Excel\ExcelRowInterface::generateColumns()
     */
    public function generateColumns(string $line)
    {
        return explode(config('excel.csv.delimiter'), $line);
    }

    /**
     * 
     * @see \Src\Interfaces\Excel\ExcelRowInterface::defineValues()
     */
    public function defineValues(array $columns)
    {   
        $companyId = (empty($columns[0])) ? null : $columns[0];
        $employeeId = (empty($columns[1])) ? null : $columns[1];
        $employeeName = (empty($columns[2])) ? null : $columns[2];
        $date = (empty($columns[3])) ? null : $columns[3];
        $type = (empty($columns[4])) ? null : $columns[4];

        $company = $this->companyService->find((int) $companyId);
        $employee = null;

        if (!empty($company)) {
            $employee = $this->employeeService->buildEmployee($employeeId, $employeeName, $type, $companyId, $date);
        }

        return compact('employee', 'company', 'date', 'type');
    }

}
