<?php

namespace Src\Companies\DB1\Handlers;

use Src\Interfaces\Handlers\ValidationHandlerInterface;
use Src\Traits\LogsTrait;

class ValidationHandler implements ValidationHandlerInterface
{

    use LogsTrait;

    /**
     * 
     * @see \Src\Interfaces\Handlers\ValidationHandlerInterface::handle()
     */
    public function handle(array $validations)
    {
        $valids = [];
        $this->logs = [];

        foreach ($validations as $key => $validation) {

            $isFailEmployee = $validation['make_employee']->fails();
            $isFailCompany = $validation['make_company']->fails();

            $valid = (!$isFailEmployee && !$isFailCompany);

            if ($isFailEmployee) {
                $this->appendLog('employee', $validation, $key);
                $this->logs[$key]['company_fails'] = ($isFailCompany || empty($validation['row']->get('company'))) ? 'true' : 'false';
            }

            if ($isFailCompany) {
                $this->appendLog('company', $validation, $key);
            }

            if ($valid) {
                $valids[] = $validation['row'];
            }
        }

        return array(
            'logs' => $this->getLogs(),
            'valids' => $valids
        );
    }

    /**
     * 
     * @param string $entity
     * @param array $row
     * @param int $key
     */
    private function appendLog(string $entity = 'employee', array $row, int $key)
    {
        $model = $row['row']->get($entity);

        $this->logs[$key][$entity . '_id'] = (empty($model)) ? 'null' : $model->getId();
        $this->logs[$key][$entity] = $row['make_' . $entity]->messages();
    }

}
