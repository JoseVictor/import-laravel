<?php

namespace Src\Companies\DB1\Validators;

use Src\Validators\AbstractValidator;
use Src\Validators\CompanyValidator;
use Src\Validators\EmployeeValidator;
use Src\Models\CompanyModel;
use Src\Models\EmployeeModel;
use Src\Interfaces\Excel\ExcelValidatorInterface;

class CsvValidator extends AbstractValidator implements ExcelValidatorInterface
{

    /**
     * 
     * @see \Src\Validators\AbstractValidator::getRules()
     */
    public static function getRules()
    {
        return [];
    }

    /**
     * 
     * @see \Src\Validators\AbstractValidator::getRules()
     */
    public static function make(array $parsedData, array $rules)
    {
        if (empty($parsedData)) {
            return [];
        }

        $rows = [];
        foreach ($parsedData as $row) {

            $company = $row->get('company');
            $employee = $row->get('employee');

            $companyData = ($company instanceof CompanyModel) ? $company->toArray() : [];
            $employeeData = ($employee instanceof EmployeeModel) ? $employee->toArray() : [];

            $rows[] = [
                'row' => $row,
                'make_employee' => EmployeeValidator::make($employeeData, EmployeeValidator::getRules()),
                'make_company' => CompanyValidator::make($companyData, CompanyValidator::getRules())
            ];
        }

        return $rows;
    }

}
