<?php

namespace Src\Traits;

trait LogsTrait
{

    /**
     *
     * @var array 
     */
    protected $logs = [];

    /**
     * 
     * @return array
     */
    public function getLogs()
    {
        return $this->logs;
    }

}
