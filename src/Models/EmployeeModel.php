<?php

namespace Src\Models;

use Src\Models\AbstractModel;

class EmployeeModel extends AbstractModel
{

    const COLUMN_NAME = 'name';
    const COLUMN_ID = 'id';
    const COLUMN_STATUS = 'status';
    const COLUMN_COMPANY_ID = 'company_id';
    const COLUMN_PROCESSED_AT = 'processed_at';

    /**
     *
     * @var boolean 
     */
    public $timestamps = false;

    /**
     *
     * @var string 
     */
    protected $table = 'employees';

    /**
     *
     * @var array 
     */
    protected $fillable = [
        'id', 'status', 'company_id', 'name', 'processed_at'
    ];

    public function getUpdatedAtColumn()
    {
        return null;
    }

}
