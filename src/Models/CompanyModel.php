<?php

namespace Src\Models;

use Src\Models\AbstractModel;

class CompanyModel extends AbstractModel
{

    const COLUMN_ID = 'id';
    const TABLE = 'companies';

    /**
     *
     * @var string 
     */
    protected $table = 'companies';

    /**
     *
     * @var array 
     */
    protected $fillable = [
        'name', 'created_at'
    ];

}
