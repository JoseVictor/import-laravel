<?php

namespace Src\Models;

use Src\Models\AbstractModel;

class LogModel extends AbstractModel
{

    /**
     *
     * @var boolean 
     */
    public $timestamps = false;

    /**
     *
     * @var string 
     */
    protected $table = 'logs';

    /**
     *
     * @var array 
     */
    protected $fillable = [
        'type', 'json', 'created_at'
    ];

}
