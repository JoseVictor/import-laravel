<?php

namespace Src\Models;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractModel extends Model
{

    /**
     * 
     * @return int
     */
    public function getId()
    {
        return $this->getAttributeValue('id');
    }

}
