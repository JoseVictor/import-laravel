<?php

namespace Src\Repositories;

use Src\Models\AbstractModel;

abstract class AbstractRepository
{

    private $model;

    /**
     * 
     * @param  \Src\Models\AbstractModel  $model
     */
    public function __construct(AbstractModel $model)
    {
        $this->model = $model;
    }

    /**
     * 
     * @param type $id
     * @return \Illuminate\Database\Eloquent\Model.
     */
    public function find(int $id)
    {
        return $this->model->find($id);
    }

    /**
     * 
     * @param array $data
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * 
     * @param array $condition
     * @param array $data
     */
    public function updateOrCreate(array $condition, array $data)
    {
        return $this->model->updateOrCreate($condition, $data);
    }

}
