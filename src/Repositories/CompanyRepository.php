<?php

namespace Src\Repositories;

use Src\Models\CompanyModel;
use Src\Repositories\AbstractRepository;

class CompanyRepository extends AbstractRepository
{

    /**
     * 
     * @param \Src\Models\CompanyModel $model
     */
    public function __construct(CompanyModel $model)
    {
        parent::__construct($model);
    }

}
