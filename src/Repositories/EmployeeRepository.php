<?php

namespace Src\Repositories;

use Src\Models\EmployeeModel;
use Src\Repositories\AbstractRepository;

class EmployeeRepository extends AbstractRepository
{

    /**
     * 
     * @param \Src\Models\EmployeeModel $model
     */
    public function __construct(EmployeeModel $model)
    {
        parent::__construct($model);
    }

}
