<?php

namespace Src\Repositories;

use Src\Models\LogModel;
use Src\Repositories\AbstractRepository;

class LogRepository extends AbstractRepository
{

    /**
     * 
     * @param \Src\Models\LogModel $model
     */
    public function __construct(LogModel $model)
    {
        parent::__construct($model);
    }

}
