<?php

namespace Src\Support;

final class Constants
{

    const TYPE_EVENT_INCLUDE = 'I';
    const TYPE_EVENT_EXCLUDE = 'E';
    const STATUS_INCLUDE = 1;
    const STATUS_EXCLUDE = 0;
    const MSG_EXCEPTION_NOT_FOUND_DATA_VALID_TO_PROCESS = 'Not was found data valid to process, check the registers before';
    const MSG_EXCEPTION_INVALID_TYPE = 'Invalid type, only are allowed [' . self::TYPE_EVENT_INCLUDE . ',' . self::TYPE_EVENT_EXCLUDE . ']';

}
