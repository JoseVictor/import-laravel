<?php

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase
{

    protected $testedClass;
    protected $reflection;

    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    protected function createReflection($class = null)
    {
        if (empty($class)) {
            return $this->reflection = new \ReflectionClass(get_class($this->testedClass));
        }

        return new \ReflectionClass(get_class($class));
    }

    protected function createTemporaryFile($content = '')
    {
        $tmpfname = tempnam(sys_get_temp_dir(), 'FOO');

        $handle = fopen($tmpfname, "w");
        fwrite($handle, $content);
        fclose($handle);

        return $tmpfname;
    }

}
