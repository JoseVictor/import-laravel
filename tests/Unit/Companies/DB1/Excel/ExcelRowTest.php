
<?php

class ExcelRowTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();

        $this->dependencies = [
            'employee' => \Mockery::mock(\Src\Services\EmployeeService::class),
            'company' => \Mockery::mock(\Src\Services\CompanyService::class),
        ];

        $this->testedClass = new \Src\Companies\DB1\Excel\ExcelRow(
                ...array_values($this->dependencies)
        );

        $this->createReflection();
    }

    public function testConstruct()
    {
        $employeeProperty = $this->reflection->getProperty('employeeService');
        $employeeProperty->setAccessible(true);
        $this->assertSame($employeeProperty->getValue($this->testedClass), $this->dependencies['employee']);

        $companyProperty = $this->reflection->getProperty('companyService');
        $companyProperty->setAccessible(true);
        $this->assertSame($companyProperty->getValue($this->testedClass), $this->dependencies['company']);

        $this->assertInstanceOf(\Src\Companies\DB1\Excel\ExcelRow::class, $this->testedClass);
    }

    public function testGetNullWithInvalidKey()
    {
        $this->assertNull($this->testedClass->get('a'));
    }

    public function testGetWithValidKey()
    {
        $struct = $this->reflection->getProperty('struct');
        $struct->setAccessible(true);
        $struct->setValue($this->testedClass, ['name' => 'value']);

        $this->assertEquals('value', $this->testedClass->get('name'));
    }

    public function testGetFull()
    {
        $struct = $this->reflection->getProperty('struct');
        $struct->setAccessible(true);
        $struct->setValue($this->testedClass, ['name' => 'value']);

        $this->assertEquals(['name' => 'value'], $this->testedClass->get());
    }

    public function testSet()
    {
        $method = $this->reflection->getMethod('set');
        $method->setAccessible(true);

        $this->assertInstanceOf(\Src\Companies\DB1\Excel\ExcelRow::class, $this->testedClass->set());

        $struct = $this->reflection->getProperty('struct');
        $struct->setAccessible(true);
        $this->assertEquals([], $struct->getValue($this->testedClass));

        $line = $this->reflection->getProperty('line');
        $line->setAccessible(true);
        $this->assertEquals('', $line->getValue($this->testedClass));
    }

    public function testSetWithValues()
    {
        $this->testedClass->set([1], 'ab');

        $struct = $this->reflection->getProperty('struct');
        $struct->setAccessible(true);
        $this->assertEquals([1], $struct->getValue($this->testedClass));

        $line = $this->reflection->getProperty('line');
        $line->setAccessible(true);
        $this->assertEquals('ab', $line->getValue($this->testedClass));
    }

    public function testGetInstance()
    {
        $this->dependencies['company']
                ->shouldReceive('find')
                ->with(10)
                ->andReturn(null);

        $result = $this->testedClass->getInstance('10');

        $this->assertInstanceOf(\Src\Companies\DB1\Excel\ExcelRow::class, $result);

        $newReflection = $this->createReflection($result);
        $struct = $newReflection->getProperty('struct');
        $struct->setAccessible(true);

        $this->assertEquals([
            'employee' => null,
            'company' => null,
            'date' => null,
            'type' => null
                ], $struct->getValue($result));
    }

    public function testGenerateColumns()
    {
        $string = implode(config('excel.csv.delimiter'), ['a', 'b']);

        $this->assertEquals(['a', 'b'], $this->testedClass->generateColumns($string));
    }

    public function testDefineValuesEmpty()
    {
        $this->dependencies['company']
                ->shouldReceive('find')
                ->with(10)
                ->once()
                ->andReturn(null);

        $this->dependencies['employee']
                ->shouldReceive('buildEmployee')
                ->never();

        $result = $this->testedClass->defineValues([10]);

        $this->assertEquals([
            'employee' => null,
            'company' => null,
            'date' => null,
            'type' => null
                ], $result);
    }

    public function testDefineValues()
    {
        $this->dependencies['company']
                ->shouldReceive('find')
                ->with(1)
                ->andReturn('123');

        $this->dependencies['employee']
                ->shouldReceive('buildEmployee')
                ->once()
                ->with(2, 3, 5, 1, 4)
                ->andReturn('456');

        $result = $this->testedClass->defineValues([1, 2, 3, 4, 5]);

        $this->assertEquals([
            'employee' => '456',
            'company' => '123',
            'date' => 4,
            'type' => 5
                ], $result);
    }

}
