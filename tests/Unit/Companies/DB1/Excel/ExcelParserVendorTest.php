<?php

class ExcelParserVendorTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();

        $this->dependencies = [
            'row' => \Mockery::mock(\Src\Companies\DB1\Excel\ExcelRow::class),
        ];

        $this->otherDependencies = [
            'reader' => \Mockery::mock(\Maatwebsite\Excel\Readers\LaravelExcelReader::class)
        ];

        $this->testedClass = new \Src\Companies\DB1\Excel\ExcelParserVendor(
                ...array_values($this->dependencies)
        );

        $this->createReflection();
    }

    public function testConstruct()
    {
        $excelProperty = $this->reflection->getProperty('excelRow');
        $excelProperty->setAccessible(true);

        $this->assertInstanceOf(\Src\Companies\DB1\Excel\ExcelParserVendor::class, $this->testedClass);
        $this->assertSame($excelProperty->getValue($this->testedClass), $this->dependencies['row']);
    }

    public function testParseException()
    {
        $this->otherDependencies['reader']
                ->shouldReceive('getFileName')
                ->with()
                ->once();

        $this->expectExceptionMessage('Filename cannot be empty');
        $this->expectException(\ErrorException::class);

        $this->testedClass->parse($this->otherDependencies['reader']);
    }

    public function testParseEmptyLines()
    {
        $this->otherDependencies['reader']->file = $this->createTemporaryFile();

        $this->otherDependencies['reader']
                ->shouldReceive('getFileName')
                ->andReturn('abc')
                ->with()
                ->once();

        $this->dependencies['row']
                ->shouldReceive('getInstance')
                ->never();

        $result = $this->testedClass->parse($this->otherDependencies['reader']);
        $this->assertInstanceOf(\Src\Companies\DB1\Excel\ExcelParserVendor::class, $result);
    }

    public function testParse()
    {
        $string = implode(config('excel.csv.line_ending'), ['abc', 'zxc']);
        
        $this->otherDependencies['reader']->file = $this->createTemporaryFile($string);

        $this->otherDependencies['reader']
                ->shouldReceive('getFileName')
                ->andReturn('abc')
                ->with()
                ->once();

        $this->dependencies['row']
                ->shouldReceive('getInstance')
                ->with('abc')
                ->andReturn(1)
                ->once();

        $this->dependencies['row']
                ->shouldReceive('getInstance')
                ->with('zxc')
                ->andReturn(2)
                ->once();

        $result = $this->testedClass->parse($this->otherDependencies['reader']);
        $this->assertInstanceOf(\Src\Companies\DB1\Excel\ExcelParserVendor::class, $result);

        $dataProperty = $this->reflection->getProperty('data');
        $dataProperty->setAccessible(true);

        $this->assertEquals([
            'filename' => 'abc',
            'data' => [1, 2],
                ], $dataProperty->getValue($this->testedClass));
    }

    public function testReadEmpty()
    {
        $this->otherDependencies['reader']->file = $this->createTemporaryFile();

        $result = $this->testedClass->read($this->otherDependencies['reader']);
        $this->assertEquals([], $result);
    }

    public function testRead()
    {
        $string = implode(config('excel.csv.line_ending'), ['abc', 'zxc']);
        $this->otherDependencies['reader']->file = $this->createTemporaryFile($string);

        $result = $this->testedClass->read($this->otherDependencies['reader']);
        $this->assertEquals(['abc', 'zxc'], $result);
    }

    public function testEncodeUtf8()
    {
        $str = 'eucjp-win';

        $this->assertEquals($str, $this->testedClass->encode($str));
    }

    public function testEncodeIso88591()
    {
        $str = 'áéóú,a';

        $result = $this->testedClass->encode($str);

        $this->assertEquals(mb_convert_encoding($str, 'UTF-8', 'ISO-8859-1'), $result);
    }

    public function testGet()
    {
        $dataProperty = $this->reflection->getProperty('data');
        $dataProperty->setAccessible(true);
        $dataProperty->setValue($this->testedClass, 'abc');

        $this->assertEquals($this->testedClass->get(), 'abc');
    }

}
