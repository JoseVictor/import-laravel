# Aplicação de importação
A proposta dessa aplicação é ler, processar e persistir dados no banco de dados referente à arquivos localizados
em uma pasta.

### Configurando o projeto
Se seu ambiente estiver todo configurado (PHP,SERVER, ...), então iremos executar alguns comandos para obter/usar o projeto :

Navegue até o local do servidor onde a aplicação ficará:
```sh
$ cd /servidor/www/html;
```

### Definindo pasta do projeto
Ao clonar um projeto, se passado o terceiro paramêtro, então ele será o nome da pasta, caso contrário, o nome da pasta será o nome do repositório `.git`:
```sh
#Clonando
$ git clone git@bitbucket.org:JoseVictor/import-laravel.git import-laravel;
Cloning into 'import-app'...
remote: Counting objects: 482, done.
remote: Compressing objects: 100% (388/388), done.
remote: Total 482 (delta 224),Re reused 139 (delta ce35)
Receiving objects: 100% (482/482), 208.57 KiB | 73.00 KiB/s, done.
Resolving deltas: 100% (224/224), done.
Checking connectivity... done.
```

### Checando existência da estrutura do projeto
Se todos os arquivos foram baixados corretamente, a listagem deverá ficar assim:
```sh
$ ls;
artisan*    composer.json  config/    gulpfile.js  package.json  public/  resources/  server.php  storage/
bootstrap/  composer.lock  database/  phpunit.xml  readme.md  routes/  src/  tests/
```

### Instalação do projeto : 
Agora vamos instalar todas as dependências do projeto usando o composer:

```sh
$ composer install;
Loading composer repositories with package information
Installing dependencies (including require-dev) from lock file
Warning: The lock file is not up to date with the latest changes in composer.json. You may be getting outdated dependencies. Run update to update them.
Package operations: 64 installs, 0 updates, 0 removals
  - Installing nikic/php-parser (v3.0.6): Loading from cache
  - Installing classpreloader/classpreloader (3.1.0): Loading from cache
  - Installing doctrine/inflector (v1.2.0): Loading from cache
  ...
```

### Definindo varíaveis de desenvolvimento : 
```sh
$ cp .env.example .env;
$ vim .env;
```

Após o comando vim (comando para editar um arquivo pelo terminal), será exibido uma tela como essa :
  - Digite `i` = Habilitar modo de escrita
  - Aperte `ESC` = Sair do modo de escrita
  - Digite `wq` = Sair e salvar as alterações
```sh
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=import
DB_USERNAME=root
DB_PASSWORD=
...
```

Segue uma lista de varíaveis que variam muito de projeto para projeto, fique à vontade para altera-las :

```sh
[APP_URL, DB_CONNECTION, DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD]
```

### Definindo permissões para a aplicação: 
Dependendo do usuário que estiver logado, terá que dar permissão para a aplicação conseguir escrever por exemplo em `cache`, para dar total liberdade para a app, execute o seguinte comando dentro da pasta do projeto :

```sh
$ cd ..;
$ chmod -R 777 import-laravel/;
```

### Inicializando 
Definindo uma nova chave para a aplicação:
```sh
$ cd import-laravel;
$ php artisan key:generate;
Application key [base64:AZvinECagxyjCiLD1dDqqo8T1/d7JXuF+T3+WX40W0g=] set successfully.
```

Crie as tabelas e popule alguns dados usando os `migrations` e `seeders`: 
```sh
$ php artisan migrate --seed;
Migration table created successfully.
Migrated: 2014_10_12_000000_create_log_table
Migrated: 2017_07_25_234242_create_companies_table
Migrated: 2017_07_25_234448_create_employees_table
Seeding: CompaniesTableSeeder
```

Obs : Dentro do arquivo `database\seeds\CompaniesTableSeeder` contém a lista de empresas que serão registradas no momento da configuração do projeto, no exemplo de `Employee/Company`, caso não exista uma empresa, então o funcionário será considerado como invalído. 

### Pronto, agora só usufruir da app
Crie uma pasta para guardar os arquivos:
  - `Atenção` : Caso deseje criar pastas com nomes e lugares diferentes, altere o arquivo (`Src\Services\FileService.php`)
```sh
$ cd storage/app/public/;
$ mkdir docs && cd docs/;
$ mkdir files-processeds;
$ mkdir files-to-process;
```

Execute o comando no `console` para conseguir processar os arquivos .csv caso eles existam:
```sh
$ php artisan command:import;
Array
(
    [type] => FAIL
    [json] => {"msg":"No files to process"}
)
```

### Logs
Os logs são armazenados em dois lugares :
  - `storage/logs/laravel.log`
  - `database`

Exemplo de log printado no terminal:
```sh
Array
(
    [type] => FAIL
    [json] => {
        "employ_id": 88290,
        "employ": {
            "processed_at": [
                "The processed at is not a valid date.",
                "The processed at field is required."
            ]
        },
        "company_fails": "false"
}

```

Exemplo de log salvo no arquivo `laravel.log`:
```sh
[2017-11-12 13:11:22] local.ERROR: {
    "type": "FAIL",
    "json": "{
    \"employ_id\": 19385,
    \"employ\": {
        \"company_id\": [
            \"The company id must be a number.\"
        ]
    }
}
```

### Testes
Navegue até a raiz do projeto, e então execute:
```sh
$ vendor/bin/phpunit

PHPUnit 5.7.21 by Sebastian Bergmann and contributors.
...................                                              19 / 19 (100%)
Time: 382 ms, Memory: 12.00MB
OK (19 tests, 28 assertions)
```

# Tecnologias utilizadas
Todas essas listadas são usadas no 'server-side':

| Descrição | Link |
| ------ | ------ |
| PHP 7.0.10 | [https://secure.php.net/manual/pt_BR/index.php]
| Laravel 5.3.* | [https://laravel.com/docs/5.3]
| Git | [https://git-scm.com/]
| Composer | [https://getcomposer.org/]

# Bibliotecas auxiliares

| Descrição | Link |
| ------ | ------ |
| Laravel excel | [http://www.maatwebsite.nl/laravel-excel/docs]

### Conclusão
Lembre-se sempre do comando de definir as permissões, poderá ser utíl mais de uma vez ! Obrigado.