<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Src\Models\CompanyModel;
use Carbon\Carbon;

class CompaniesTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['id' => 6854, 'name' => 'Company #1', 'created_at' => Carbon::now()],
            ['id' => 7199, 'name' => 'Company #2', 'created_at' => Carbon::now()]
        ];

        DB::table(CompanyModel::TABLE)->insert($data);
    }

}
