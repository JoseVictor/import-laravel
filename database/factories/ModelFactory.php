<?php

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | Here you may define all of your model factories. Model factories give
  | you a convenient way to create models for testing and seeding your
  | database. Just tell the factory how a default model should look.
  |
 */

$factory->define(\Src\Models\EmployModel::class, function (Faker\Generator $faker) {

    return [
        'id' => $faker->numberBetween(1, 10),
        'name' => $faker->name,
        'company_id' => $faker->numberBetween(1, 10),
        'status' => $faker->numberBetween(0, 1),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'processed_at' => $faker->date('Y-m-d H:i:s')
    ];
});
